FROM alpine

RUN apk add --update --no-cache lighttpd \
	&& rm -rf /var/cache/apk/*

EXPOSE 80

CMD ["lighttpd", "-D", "-f", "/etc/lighttpd/lighttpd.conf"]
